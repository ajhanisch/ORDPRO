import os
import sys
import time
import getpass
import hashlib
import logging
import argparse

class Setup:
	'''
	VARIABLES
	'''
	version = '0.1'
	program = os.path.basename(__file__)
	user = getpass.getuser()
	repository = 'https://gitlab.com/ajhanisch/ORDPRO'
	date = time.strftime('%Y-%m-%d_%H-%M-%S')

	'''
	DIRECTORIES
	'''
	directory_working = os.getcwd()
	directory_working_log = os.path.join(directory_working, 'LOGS', date)

	'''
	FILES
	'''
	file_working_log = os.path.join(directory_working_log, '{}_{}.log'.format(date, program))
	
	'''
	DICTIONARIES
	'''
	dict_directories = {
		'directory_working' : directory_working,
		'directory_working_log' : directory_working_log
	}

	
	'''
	CREATE ARGUMENT PARSER
	'''
	parser = argparse.ArgumentParser(description='Script to replace/remove PII from versions <= 3.7 of ORDPRO.')
	
	'''
	ARGUMENTS
	'''
	parser.add_argument(
		'--input',
		nargs = '+',
		required = True,
		help = r'Full path to orders directory. You can pass both FULL paths to ORDERS_BY_SOLDIERS and UICS separated by space.'
	)
	parser.add_argument(
					'--verbose',
					choices=[ 'debug', 'info', 'warning', 'error', 'critical' ],
					default='info',
					help='Enable specific program verbosity. Default is info. Set to debug for complete script processing in logs and screen. Set to warning or critical for minimal script processing in logs and screen.'
	)
	parser.add_argument(
					'--version',
					action='version',
					version='[{}] - Version [{}]. Check [{}] for the most up to date information.'.format(program, version, repository)
	)

	args = parser.parse_args()

def replace_pii(dir_input):
	salt = 'd86d4265-842e-4a4a-b9d8-e6a6961bcfab' # Generated using str(uuid.uuid4()) on 2/23/2018 @ 1000
	
	logging.info('Processing all input in {}.'.format(dir_input))
	for p in dir_input:
		if os.path.isdir(p):
			logging.info('Processing [{}] now.'.format(p))
			for root, subdirs, files in os.walk(p):
				for file in files:
					uid_old = root.split('\\')[-1].split('___')[-1]
					uid = (hashlib.md5(salt.encode() + uid_old.replace('-','').encode()).hexdigest())[:10]
					file_new = file[:4] + file[18:]		
					path_new = os.path.join(root.replace(uid_old, uid))		
					'''
					Rename folder.
					'''
					try:
						if len(uid_old) > 10: # Len of uid_old will == 10 when renamed to new unique string. It will be more than 10 when it is still an SSN.
							logging.debug('Directory [{}] still has SSN as UID. Changing [{}] to [{}] now.'.format(root, uid_old, uid))
							os.rename(root, path_new)
							logging.info('Changed directory [{}] to [{}] successfully!'.format(root, uid))
					except FileNotFoundError: # This will occur when trying to rename the same folder (when a single folder has multiple order files in it)
						logging.debug('Directory [{}] is already renamed to [{}].'.format(root, path_new))
					'''
					Rename file.
					'''
					if len(file.split('___')) == 6: # Len of file.split will == 6 when an SSN is included in the file name.
						logging.debug('File [{}] still has an SSN. Removing [{}] now.'.format(os.path.join(root, file), uid_old))
						os.rename(os.path.join(path_new, file), os.path.join(path_new, file_new))
						logging.info('Removed [{}] from file [{}] successfully!'.format(uid_old, os.path.join(root, file)))
					else:
						logging.debug('File [{}] is already renamed to [{}].'.format(os.path.join(root, file), file_new))
			logging.info('Finished processing [{}] now.'.format(p))
		else:
			logging.critical('{} is not a directory. Try again with proper input.'.format(p))
			sys.exit()
	logging.info('Finished processing {} now.'.format(dir_input))
	
def main():
	'''
	Main function. Everything starts here.
	'''
	setup = Setup()
	args = setup.args
	'''
	Create required directories from setup.
	'''
	for key, value in setup.dict_directories.items():
		if not os.path.exists(value):
			os.makedirs(value)
	'''
	Setup logging.
	'''
	dict_levels = {
		'debug': logging.DEBUG,
		'info': logging.INFO,
		'warning': logging.WARNING,
		'error': logging.ERROR,
		'critical': logging.CRITICAL,
	}
	level_name = args.verbose
	level = dict_levels.get(level_name)
	format = '[%(asctime)s] - [%(levelname)s] - %(message)s'
	handlers = [logging.FileHandler(setup.file_working_log), logging.StreamHandler()]
	logging.basicConfig(
		level = level,
		format = format,
		handlers = handlers
	)
	'''
	Present what arguments and parameters are being used. Useful for developer and user of script to easily start troubleshooting by having as much info in logs as possible.
	'''
	logging.info('Hello [{}]! You are running [{}] with the following arguments: '.format(setup.user, setup.program))
	for a in args.__dict__:
		logging.info(str(a) + ' : ' + str(args.__dict__[a]))
	'''
	Argument handling.
	'''
	if args.input:
		replace_pii(args.input)
		sys.exit()

'''
Entry point of script.
'''
if __name__ == '__main__':
	main()